<?php

namespace Drupal\neato_companion\Commands;

use Drush\Commands\DrushCommands;

/**
 * Neato theme Drush ^9 commands.
 */
class NeatoCompanionCommands extends DrushCommands {

  /**
   * Echos back hello with the argument provided.
   *
   * @param string $name
   *   Argument provided to the drush command.
   *
   * @command neato_companion:hello
   * @aliases neato-hello
   * @options arr An option that takes multiple values.
   * @options msg Whether or not an extra message should be displayed to the user.
   * @usage neato_companion:hello akanksha --msg
   *   Display 'Hello Akanksha!' and a message.
   */
  public function hello($name, $options = ['msg' => FALSE]) {
    if ($options['msg']) {
      $this->output()->writeln('Hello ' . $name . '! This is your first Drush 9 command.');
    }
    else {
      $this->output()->writeln('Hello ' . $name . '!');
    }
  }

}